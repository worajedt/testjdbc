package com.ccc;

import java.sql.*;

/**
 * Created by wrj on 7/9/2016 AD.
 */
public class TestJDBC {

    private static String USER;
    private static String PASSWORD;
    private static String URL;
    private static Statement stmt;
    private static ResultSet rs;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
//        STEP 1 find driver class
        Class.forName("org.h2.Driver");

//        STEP 2 create connection
        Connection conn = null;
        try {
            USER = "sa";
            PASSWORD = "";
            URL = "jdbc:h2:tcp://localhost/~/thaicollate";
            conn = DriverManager
                    .getConnection(URL, USER, PASSWORD);

//        STEP 3 create sql statement and query
            stmt = conn.createStatement();
            String sql = "SELECT id, name FROM TESTJDBC";
            rs = stmt.executeQuery(sql);

//        STEP 4 print output
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                System.out.println("ID:" + id + " Name: " + name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        rs.close();
        stmt.close();
        conn.close();
    }
}
